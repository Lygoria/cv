import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
  constructor(private translateService: TranslateService) {
    translateService.addLangs(['en', 'fr']);
    translateService.setDefaultLang('en');
    translateService.use(translateService.getBrowserLang() ?? 'en');
  }
  
  printStaticCv(): void {
    window.print();
  }

  updateLang(lang: string): void {
    this.translateService.use(lang)
  }
  
}
