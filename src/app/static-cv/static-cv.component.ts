import { Component } from '@angular/core';

@Component({
  selector: 'static-cv',
  templateUrl: './static-cv.component.html',
  styleUrls: ['./static-cv.component.scss']
})
export class StaticCvComponent {
  skillLevels: {[level: string]: {number: number, color: string}} = {
    'intermediate': {
      'number': 3,
      'color': 'info'
    },
    'advanced': {
      'number': 4,
      'color': 'primary'
    },
    'expert': {
      'number': 5,
      'color': 'success'
    }
  }
}
