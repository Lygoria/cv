import { TitleCasePipe, registerLocaleData } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import localeEn from '@angular/common/locales/en';
import localeFr from '@angular/common/locales/fr';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppComponent } from './app.component';
import { StaticCvComponent } from './static-cv/static-cv.component';
import { fontAwesomeIcons } from './utils/icons';
import { LocalizedDatePipe } from './utils/localized-date.pipe';
import { CustomDateComponent } from './utils/date.component';
import { NgbProgressbarModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    StaticCvComponent,
    CustomDateComponent,
    LocalizedDatePipe,
  ],
  imports: [
    BrowserModule,
    FontAwesomeModule,
    NgbProgressbarModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    TitleCasePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(iconLibrary: FaIconLibrary) {
    registerLocaleData(localeEn, 'en');
    registerLocaleData(localeFr, 'fr');
    iconLibrary.addIcons(...fontAwesomeIcons)
  };
}

export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http);
}