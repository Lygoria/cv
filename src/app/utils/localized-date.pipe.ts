import { DatePipe, TitleCasePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Pipe({
  name: 'localizedDate',
  pure: false
})
export class LocalizedDatePipe implements PipeTransform {
  
  constructor(private translateService: TranslateService, private titleCasePipe: TitleCasePipe) {}

  transform(value: any, pattern: string = 'MMM yyyy'): any {
    return this.titleCasePipe.transform(new DatePipe(this.translateService.currentLang).transform(value, pattern));
  }
}