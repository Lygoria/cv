import {
    faCakeCandles,
    faPhone,
    faEnvelope,
    faCaretRight,
    faArrowTurnDown,
    faToolbox,
    faPrint,
    faCircle as faSolidCircle
} from '@fortawesome/free-solid-svg-icons';

import {
    faCircle as faRegularCircle,
} from '@fortawesome/free-regular-svg-icons';

export const fontAwesomeIcons = [
    faCakeCandles,
    faPhone,
    faEnvelope,
    faCaretRight,
    faArrowTurnDown,
    faToolbox,
    faPrint,
    faSolidCircle,
    faRegularCircle,
];
