import { Component, Input } from '@angular/core';

@Component({
  selector: 'custom-date',
  template: `<div *ngIf="startDate && endDate" class="float-end fst-italic">
    {{ startDate | localizedDate }}&nbsp;<fa-icon icon="arrow-turn-down" size="sm"></fa-icon>
    <div class="text-end" *ngIf="endDate !== 'today'">{{ endDate | localizedDate }}</div>
    <div class="text-end" *ngIf="endDate === 'today'">{{ endDate | translate | titlecase }}</div>
  </div>`
})
export class CustomDateComponent {
  @Input() startDate!: string;
  @Input() endDate!: string;
}
